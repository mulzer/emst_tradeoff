﻿------------------------------------------------------
Reviewer G:

- site -> point-site or point?
A: We have resolved the inconsistency.

- L134, what does "the corresponding connected component" mean?

A: We have removed the redundant word "corresponding".

- L139, what does "the first Delaunay edge pr of length less than |e|" mean?
If I understand it correctly, one endpoint of this edge is not necessarily
p.

A: we have substituted "first" with "a". One endpoint of the edge
will alway be p, the other endpoint rotates

- L187, "each face in the graph or the outer face of each connected
component of the graph" First of all, what does “the outer face of a
connected component" mean? Also, is it a face of the graph? If so, why do
you distinguish it from the other faces.

A: A planar graph divides the plane into regions, called faces. One of these
faces is unbounded, and is called the outer face. Here, our planar graph
has several (planar) components. Consider each component as
one single graph. Then, that component has an outer face, which 
we refer to as the "outer face of a connected component". 
We have added a footnote to clarify why we distinguish them.

- L209, what is meant by "an s-workspace algorithm"? Is the size of your
workspace exactly s, or O(s)?

A: We have exchanged the term "s-workspace" with O(s) cells of workspace.
The size of workspace is mentioned in L52 as O(s) cells.

- L266, we can compute RNG(S) -> we can compute the edges of RNG(S)?

A: Done.

- L344, sorted order -> increasing order with respect to their lengths?

A: Done.

- L419, the first sentence should be rewritten. Otherwise, H might contain
edges other than net-edges, batch-edges, and successors of batch-edges, and
thus |H| might be larger than O(s).

A: Resolved.

- This algorithm returns the edges of the EMST in the increasing order with
respect to their lengths. But it seems more natural to report the edges in
clockwise order along the boundary of each cycle. If it is possible, it
would be better to mention how to do this.

A: The output has to be reported while being computed, since there is a 
limitation in the workspace and they cannot be stored. One might achieve 
reporting the edges in special orders by several rounds of computing edges
and in each round only report some desired edges. This is computationally
more expensive.


------------------------------------------------------

------------------------------------------------------
Reviewer I:

Line 98: Why is EMST(S) unique? Is it by a known result or does it follow
from the preceding text? I don't see how it follows from the assumptions. If
you assume that all sets of pairwise distances have distinct sums, it would
follow trivially, but you only assume the individual distances to be
distinct.

A: It is known that, if each edge of a weighted graph G has a distinct 
weight then there will be only one, unique minimum spanning tree. 
Please refer to [15].

Line 159: I would not include the "and only if" here. You usually don't do
that in definitions.

A: Done.

Line 310: E_R is only defined inside Lemma 3.3. I feel that it should be
defined more globally.

A: The definition of E_R is in L176.

Line 324: Here you talk about the next edge of $\overrightarrow{e_j}$ on
$F$. At line 203 you defined the next edge on $F$ FOR
$\overrightarrow{e_j}$, so you might want to use this wording consistently.
The different wording also appears other places.

A: Resolved.

Line 395: Here starts a new sentence as part of property (ii), which is
itself only a part of a sentence also including property (i). I think it is
better if each of these properties starts with a new sentence, so that part
(ii) can consist of two full sentences.

A: Done.

Line 424: "per each" is not good.

A: We have removed "each".

Line 446: The sentence starting here confused me a bit. Can you elaborate on
this? Why can there be more connected components in G? And can't there also
be some connected components of RNG_i that are not in G? Because only long
face cycles of RNG_i are represented by edges in the s-net, so there could
be many short cycles that do not appear in G.

A:  We rephrased the sentence and added an explanatory footnote:
"Two (or several) face-cycles in one component of RNG_i may share some vertices.
However, these vertices need not necessarily appear as vertices in G. Hence,
representing those face-cycles with compressed edges, one might not represents 
their common parts in G. Therefore, such face-cycles might belong to distinct 
components in G."

A: For the question "can't there also be some connected components of RNG_i 
that are not in G?": actually, any face-cycle and therefore any connected 
component of RNG_i that have some effect in processing the next batch-edges 
are represented in G, irrespective of if they are small face-cycle or not. 
The reason is that all the successors of the batch-edges are in G. Therefore, 
if there are two components of RNG_i that might get connected to each other 
by a batch-edge, both such components have been properly represented in G. 
We agree that there might be some other components in RNG_i that are not 
represented in G in some step, but their existence has no effect at that step.

Line 464: What are H-edges? Only defined in the proof of Lemma 5.2, I think.

A: In L458, we have mentioned that the Graph G is as it is described in Lemma 5.2.

Line 534: 2d should be 2D.

A: Done.

